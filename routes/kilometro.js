var express = require('express');
var app = express();
var distance = require('google-distance-matrix'); 
// distance.transit_routing_preference('fewer_transfers');
distance.key('AIzaSyDZmUkCucQOodBJ3hul-wHsHwsUK2_UmmU');
distance.units('metric');
var mdAutentication = require('../middlewares/autenticacion');

var Distance = require('../models/distance');
 

app.post('/',mdAutentication.verificaToken, (req, res) => { 
    var body = req.body;   
	// var origins = ['San Francisco CA'];
    // var destinations = ['New York NY', '41.8337329,-87.7321554'];
	var origins = body.origins;;
    var destinations = body.destinations;
    list = [];
	totalDistancia = 0;    


    distance.matrix(origins, destinations, function (err, distances) {
        if (err) {
            return console.log(err);
        }
        if(!distances) {
            return console.log('no distances');
        }
        if (distances.status == 'OK') {     
            for (var i=0; i < origins.length; i++) {
                for (var j = 0; j < destinations.length; j++) {
                    var origin = distances.origin_addresses[i];
                    var destination = distances.destination_addresses[j];
                    if (distances.rows[0].elements[j].status == 'OK') {
                        var distance = distances.rows[i].elements[j].distance;						
						console.log('distanceRow', distances.rows[0].elements[0]);                      
						this.totalDistancia= this.totalDistancia+ distance;
                        this.list.push({origin , destination ,distance});
                    } else {
                        console.log(destination + ' is not reachable by land from ' + origin);
                    }
                }
            }
            console.log('dist', this.totalDistancia)
 
            this.totalDistancia = this.totalDistancia/1000;


      
            res.status(200).json({
            ok: true,
            mensaje:'Correcto',
            distancia : this.list,
            totalDistancia:this.totalDistancia
          
        });
        }
    });

 }  )

module.exports = app;