var express = require('express');
var app = express();
var nodemailer = require('nodemailer');

var Moto = require('../models/motorizado');
var Usuario = require('../models/usuario');
var Pedido = require('../models/pedido');
var EmailCtrl = require('./emailRegistro');
var Direccion = require("../models/direccion");
var Motorizado = require("../models/motorizado");
var hbs = require('nodemailer-express-handlebars');



var mdAutentication = require('../middlewares/autenticacion');
// Rutas 

/* Obtener Todos los Pedidos*/
app.get('/', (req, res, next) => {

    Pedido.find({})
    .populate('direccionEntrega', 'direccion referencia')
    .populate('proveedor', 'nombre direccion')
    .populate('producto', 'nombre')
    .populate('encargadoDelivery', 'nombreApellido')
        .exec(
            (err, pedidos) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando pedido',
                        errors: err
                    })

                } 

                if (pedidos.length===0) {
                    return res.status(404).json({
                        ok: false,
                        mensaje: 'No se han registrado pedidos. Favor registre',
                        errors: err
                    })

                } 

                res.status(200).json({
                    ok: true,
                    pedidos: pedidos
                });

            })
});

/* Obtener Distinct de pedidos*/
app.get('/Distinct', (req, res, next) => {

    Pedido.distinct("nControl" )
    .populate('proveedor', 'nombre direccion')
    .populate('producto', 'nombre')
    .populate('encargadoDelivery', 'nombreApellido')
        .exec(
            (err, pedidos) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando pedido',
                        errors: err
                    })

                } 

                if (pedidos.length===0) {
                    return res.status(404).json({
                        ok: false,
                        mensaje: 'No se han registrado pedidos. Favor registre',
                        errors: err
                    })

                } 

                res.status(200).json({
                    ok: true,
                    pedidos: pedidos
                });

            })
});



/* Crear nuevos Pedidos*/
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var date = new Date();
    var usuarioData;

    var year = date.getFullYear();
    var month= date.getMonth()+1;
    var day = date.getDate();  
    var hour = date.getHours();
    var minut= date.getMinutes();
    var secon = date.getSeconds();

 
    var nControl = month.toString()+day.toString()+hour.toString()+ minut.toString()+ secon.toString();


    body.pedidos.nControl = nControl;

    body.pedidos.forEach(element => {
        element.nControl = nControl;
    });
    

    var pedido      = new Pedido({
            nControl    : nControl,
            direccionEntrega : body.direccionEntrega,
            pedidos     : body.pedidos,
            totalProductos: body.totalGeneral,
            totalDelivery : body.totalDelivery,
            totalArticulos : body.totalArticulos,
            encargadoDelivery: null,
            estatus : body.estatus, 
            usuario : req.usuario._id,  
            email :  req.usuario.email,  
            formaDePago :  body.formaDePago, 
            comprobante:  body.comprobante, 
            documento:  body.documento, 
            fechaPedido : date
        
    });

    
    pedido.save((err, pedidoGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear pedido.',
                errors: err
            })
        }

     //   EmailCtrl.sendEmail(pedidoGuardado, 'pedido');


// Busco datos de usuario segun id
  Usuario.findById(pedidoGuardado.usuario, (err, usuario) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: "Error inesperado.",
        error: err
      });
    }

    console.log('usuario', usuario);

    if (usuario.length === 0) {
        return res.status(404).json({
          ok: false,
          mensaje: "Error al validar usuario.",
          error: { message: "No existe un usuario con es ID" }
        });
      }


    // Busco direccion segun id
    Direccion.findById(pedidoGuardado.direccionEntrega, (err, direccion) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          mensaje: "Error al buscar direccion.",
          error: err
        });
      }


      console.log('sireccion', direccion);
   

    // Busco motorizado segun id
      Motorizado.findById(pedidoGuardado.encargadoDelivery, (err, motorizado) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            mensaje: "Error inesperado.",
            error: err
          });
        }

        var smtpConfig = {
            host: "smtpout.secureserver.net",
            port: 465,
            secure: true, // use SSL
            auth: {
              user: "gestiones@fastzoom.cl",
              pass: "laserena2019#"
            }
          };
        


  // Creo transporter
  let transporter = nodemailer.createTransport(smtpConfig);
  const handlebarOptions = {
    viewEngine: {
      extName: ".handlebars",
      partialsDir: "./templates/",
      layoutsDir: "./templates/",
      defaultLayout: "pedido.handlebars"
    },
    viewPath: "./templates/",
    extName: ".handlebars"
  };

  transporter.use("compile", hbs(handlebarOptions));


        var mailOptions = {
                      from: '"Fastzoom" <gestiones@fastzoom.cl>',
                      to: usuario.email,
                      subject: "Tu pedido Fastzoom",
                      //  text: 'El usuario text ' + req.nControl + ' se ha sido creado con ¡Genial!.',
                      //  html: ' <h3> Datos de tu pedido </h3> Numero de Control: ' + req.nControl + '<br> Numero de Articulos: ' + new Intl.NumberFormat().format(req.totalArticulos) + '<br> Costo de Producto:  ' + new Intl.NumberFormat().format(req.totalProductos) +' <br> Costo de Envio: ' + req.totalDelivery,
                      attachments: [
                        {
                          filename: "fast-zoom-logosinfondo.png",
                          path: "./assets/fast-zoom-logosinfondo.png",
                          cid: "gestiones@fastzoom.cl"
                        }
                      ],
                      template: "pedido",
                      context: {
                        nControl: pedidoGuardado.nControl,
                        totalArticulos: pedidoGuardado.totalArticulos,
                        costoProducto: pedidoGuardado.totalProductos,
                        costoEnvio: pedidoGuardado.totalDelivery,
                        formaDePago: pedidoGuardado.formaDePago,
                        total: parseInt(pedidoGuardado.totalProductos) + parseInt(pedidoGuardado.totalDelivery),
                        direccionText: direccion,
                        usuario: usuario
                      } // send extra values to template
                    };


  // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
        return error;
      } else {
        console.log("Email sent");
        res.status(201).json({ 
            ok: true,
            pedido: pedidoGuardado,
            mensaje: 'Le informaremos el estatus de su pedido via correo.',
        });
      }
    });

});

});

});
        
        
        // res.status(201).json({ 
        //     ok: true,
        //     pedido: pedidoGuardado,
        //     mensaje: 'Le informaremos el estatus de su pedido via correo.',
        // });
    });
});

/* Actualizar Pedido */
app.put('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    Pedido.findById(id, (err, pedido) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Pedido.',
                erros: err
            });
        }

        if (!pedido) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El pedido con el id: ' + id + 'no existe',
                erros: { message: 'No existe un pedido con este ID' }
            });
        }

            pedido.encargadoDelivery = body.idMoto,
            pedido.estatus     = body.estatus, 
            pedido.usuario = req.usuario._id,  

            pedido.save((err, pedidoGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar producto',
                    erros: err
                });
            }

            res.status(200).json({
                ok: true,
                pedido: pedidoGuardado,
                usuariotoken: req.usuario
            });
        });
    });
});


/* asignar Pedido */
app.put('/asignar/:id', mdAutentication.verificaToken, (req, res) => {

    var id = req.params.id
    var body = req.body;
    var motorizadoEmail;

     
    Moto.findById(body.idMoto, (err, motorizado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error buscar motorizado.',
                erros: err
            });
        }

             motorizado.estatus = 1;
             motorizadoEmail = motorizado.email;

             motorizado.save((err, motoGuardado) => {
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            mensaje: 'Error al actualizar estatus de  motorizado',
                            erros: err
                        });
                    }
               });

            });




            // Pedido.findById({id})
            // .populate('servicio', 'nombre imagen')
            // .populate('proveedor', 'nombre imagen')
            // .populate('producto', 'nombre imagen precio categoria estatus ingrediente')
            //      .exec(
            //          (err, afiliacion) => {
            //              if (err) {
            //                  return res.status(500).json({
            //                      ok: false,
            //                      mensaje: 'Error cargando afiliación.',
            //                      errors: err
            //                  })
            //              } res.status(200).json({
            //                  ok: true,
            //                  Afiliacion: afiliacion
            //              });
            //          })



    Pedido.findById(id,(err, pedido) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Pedido.',
                erros: err
            });
        }

        if (!pedido) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El pedido con el id: ' + id + 'no existe',
                erros: { message: 'No existe un pedido con este ID' }
            });
        }


        pedido.encargadoDelivery = body.idMoto,
        pedido.estatus     = body.estatus, 
        pedido.usuario = req.usuario._id,  
            
            pedido.save((err, pedidoGuardado) => {

                if (err) {
                    return res.status(400).json({
                        ok: false,
                        mensaje: 'Error al actualizar estatus de pedido',
                        erros: err
                    });
                }

              EmailCtrl.sendEmail(pedidoGuardado, 'asignaMoto', motorizadoEmail);



            return res.status(200).json({
                    ok: true,
                    pedido: pedidoGuardado,
                    mensaje: ' Pedido asignado'
                });
            });
    });
});



/* Borrar producto por ID */
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Producto.findByIdAndRemove(id, (err, productoBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar producto',
                errors: err
            })
        }

        if (!productoBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un producto con ese ID',
                errors: { message: 'No existe un producto con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            producto: productoBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;