var express = require('express');
var app = express();
var fs = require('fs');
const fileUpload = require('express-fileupload');


var Proveedor = require('./../models/proveedor');
var Producto = require('./../models/producto');

// default options
app.use(fileUpload());

app.put('/:tipo/:id', function (req, res, next) {

  var tipo = req.params.tipo;
  var id = req.params.id;


  // tipos valido

  var tiposValidos = ['proveedores', 'productos'];

  // if (tiposValidos.indexOf(tipo) < 0) {
  //   return res.status(400).json({
  //     ok: false,
  //     mensaje: 'Tipos de coleccion no validas.',
  //     errors: { mensaje: 'Debe seleccionar una imagen.' }
  //   });

  // }


  if (!req.files) {
    return res.status(400).json({
      ok: false,
      mensaje: 'No selecciono nada.',
      errors: { mensaje: 'Los tipos validos son ' + tiposValidos.join(', ') }

    });
  }

  let archivo = req.files.imagen;
  let archivoPropiedades = archivo.name.split('.');
  let extensionArchivo = archivoPropiedades[archivoPropiedades.length - 1];

  console.log(archivo)

  // Extensiones permitidas 
  let extensionesValidas = ['jpg', 'png', 'gif', 'jpeg'];
  if (extensionesValidas.indexOf(extensionArchivo) < 0) {

    return res.status(400).json({
      ok: false,
      mensaje: 'Extensión no válida.',
      errors: { mensaje: 'Las extensiones validas son ' + extensionesValidas.join(', ') }

    });
  }

  // Nombre de archivo a guardar  
  var nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extensionArchivo}`;

  // Mover el archivo a ruta definitiva
  var patch = `./upload/${tipo}/${nombreArchivo}`;

  archivo.mv(patch, err => {

    if (err) {

      return res.status(500).json({
        ok: false,
        mensaje: 'Error a mover archivo.',
        errors: { mensaje: 'Error a mover archivo : ' + err }

      });
    }

    subirPorTipo(tipo, id, nombreArchivo, res);

  })


});




function subirPorTipo(tipo, id, nombreArchivo, res) {

  if (tipo === 'proveedores') {

    Proveedor.findById(id, (err, proveedor) => {

      if (err) {
        return res.status(400).json({
          ok: false,
          mensaje: 'Error al buscar id.',
          errors: { mensaje: err }
        });

      }

      var patchViejo = './upload/proveedores/' + nombreArchivo;

      // Si existe elimina imagen anterior
      if (fs.existsSync(patchViejo)) {
        fs.unlink(patchViejo, (error) => {

          if (error) {
            return response.status(400).json({
              ok: false,
              mensaje: 'No se pudo eliminar la imagen',
              errors: error
            });
          }

        });
      }

      proveedor.imagen = nombreArchivo;
      proveedor.save((error, proveedorActualizado) => {
        if (error) {
          return response.status(500).json({
            ok: false,
            mensaje: 'No se pudo actualizar la imagen',
            errors: error
          });
        }

        return res.status(200).json({
          ok: true,
          mensaje: 'Imagen de proveedor actualizada',
          proveedor: proveedorActualizado,
        })
      });

    });


  }

  if (tipo === 'productos') {


    Producto.findById(id, (err, producto) => {

      if (err) {
        return res.status(400).json({
          ok: false,
          mensaje: 'Error al buscar id.',
          errors: { mensaje: err }
        });

      }

      var patchViejo = './upload/productos/' + producto.imagen;

      // Si existe elimina imagen anterior
      if (fs.existsSync(patchViejo)) {
        fs.unlink(patchViejo, (error) => {

          if (error) {
            return response.status(400).json({
              ok: false,
              mensaje: 'No se pudo eliminar la imagen',
              errors: error
            });
          }

        });
      }

      producto.imagen = nombreArchivo;
      producto.save((error, productoActualizado) => {
        if (error) {
          return response.status(500).json({
            ok: false,
            mensaje: 'No se pudo actualizar la imagen',
            errors: error
          });
        }

        return res.status(200).json({
          ok: true,
          mensaje: 'Imagen de producto actualizada',
          producto: productoActualizado,
        })
      });

    });

    return;
  }

}

module.exports = app;
