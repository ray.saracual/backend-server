var express = require('express');
var app = express();

var Proveedor = require('../models/proveedor');

var mdAutentication = require('../middlewares/autenticacion');
const fs = require('fs');
const path = require('path');


// Rutas 

/* Obtener Todos los Proveedores */
app.get('/', (req, res, next) => {

    Proveedor.find({},)
    .populate('direccion', 'lat lng direccion referencia')
        .exec(
            (err, proveedores) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando proveedores',
                        errors: err
                    })

                } res.status(200).json({
                    ok: true,
                    proveedores: proveedores
                });

            })

});



/* Crear nuevos Proveedores */
app.post('/',mdAutentication.verificaToken, (req, res) => {
    
    
    var date = new Date();
    var miliSecond  = date.getMilliseconds();
    var body = req.body;
    var proveedor = new Proveedor({
        nombre: body.nombre,
        telefono: body.telefono,
        email: body.email,
        direccion: body.direccion,
        estatus: body.estatus,
        imagen: body.imagen,
        comisionAcobrar:body.comisionAcobrar,
        observacion:body.observacion,
        infoFiscal:body.infoFiscal,
        usuario: req.usuario._id,

    })


    let base64Image = proveedor.imagen.split(';base64,').pop();
    
    let image =proveedor.nombre.trim()+'-'+miliSecond +'.png'; 

    let pathImage = path.resolve(__dirname,  `../upload/proveedores/${image}` ) 
  

    fs.writeFile(pathImage, base64Image, {encoding: 'base64'},
     function(err) {
        
        if(err){
         return res.status(400).json({
                ok: false,
                mensaje: 'Error al cargar imagen',
                errors: err
            })
        }

        proveedor.imagen=image;


    proveedor.save((err, proveedorGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear proveedor',
                errors: err,               
            })
        }        
        res.status(201).json({
            ok: true,
            proveedor: proveedorGuardado,
            mensaje:'Proveedor registrado'
        });

    });
});

});



/* Actualizar Proveedor */
app.put('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;
    var date = new Date();
    var miliSecond = date.getMilliseconds();

    Proveedor.findById(id, (err, proveedor) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar proveedor',
                erros: err
            });
        }

        if (!proveedor) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El proveedor con el id: ' + id + 'no existe',
                erros: { mensaje: 'No existe un proveedor con este ID' }
            });
        }
        
        var imagenVieja = proveedor.imagen;

        var pathViejo = path.resolve(__dirname,`../upload/proveedores/${imagenVieja}`) 

        var imagen= imagenVieja;
       
        if(body.imagen !== imagenVieja){
            let base64Image = body.imagen.split(';base64,').pop();
            imagen = id+'-'+miliSecond+'.png';
            let pathImage = path.resolve(__dirname,`../upload/proveedores/${imagen}`) ;
  

          fs.writeFile(pathImage, base64Image, {encoding: 'base64'},
           function(err) {
               
              if(err){
               return res.status(400).json({
                      ok: false,
                      mensaje: 'Error al cargar imagen',
                      errors: err
                  })
              }

            });



            if (fs.existsSync(pathViejo)) {
                fs.unlink(pathViejo, (error) => {
        
                  if (error) {
                    return response.status(400).json({
                      ok: false,
                      mensaje: 'No se pudo eliminar la imagen',
                      errors: error
                    });
                  }
        
                });
              }

        }
      

            proveedor.nombre = body.nombre,
            proveedor.telefono = body.telefono,
            proveedor.email = body.email,
            proveedor.direccion = body.direccion,
            proveedor.estatus = body.estatus,
          //  proveedor.imagen =body.imagen,
            proveedor.imagen = imagen,
            proveedor.comisionAcobrar=body.comisionAcobrar,
            proveedor.observacion = body.observacion
            proveedor.infoFiscal = body.infoFiscal,
            proveedor.usuario= req.usuario._id,  
                   
        proveedor.save((err, proveedorGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar proveedor',
                    erros: err
                });
            }

                res.status(200).json({
                    ok: true,
                    proveedor: proveedorGuardado,
                    mensaje:'Proveedor actualizado'
                });
        });

   
    });
});


/* Borrar proveedor por ID */
app.delete('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Proveedor.findByIdAndRemove(id, (err, proveedorBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar proveedor',
                errors: err
            })
        }

        if (!proveedorBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un proveedor con ese ID',
                errors: { message: 'No existe un proveedor con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            proveedor: proveedorBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;