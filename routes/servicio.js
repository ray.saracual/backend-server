var express = require('express');
var app = express();

var Servicio = require('../models/servicio');

var mdAutentication = require('../middlewares/autenticacion');

// Rutas 
/* Obtener Todos los Servicios */
app.get('/', (req, res, next) => {
    Servicio.find({},)
        .exec(
            (err, servicios) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando servicios',
                        errors: err
                    })
                } res.status(200).json({
                    ok: true,
                    servicios: servicios
                });
            })
});



/* Crear nuevo Servicios */
app.post('/',mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var servicio = new Servicio({
        nombre: body.nombre,
        img: body.img,
        estatus: body.estatus,
        observacion: body.observacion
    })



    servicio.save((err, servicioGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear servicio',
                errors: err
            })
        }
        res.status(201).json({
            ok: true,
            servicio: servicioGuardado
        });

    });

});



/* Actualizar Servicio */
app.put('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    Servicio.findById(id, (err, servicio) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar servicio',
                erros: err
            });
        }

        if (!servicio) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El servicio con el id: ' + id + 'no existe',
                erros: { message: 'No existe un servicio con este ID' }
            });
        }

        servicio.nombre= body.nombre,
        servicio.img = body.img,
        servicio.estatus= body.estatus,
        servicio.observacion= body.observacion

        servicio.save((err, servicioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar servicio',
                    erros: err
                });
            }

                res.status(200).json({
                    ok: true,
                    servicio: servicioGuardado,
                    usuariotoken: req.usuario
                });
        });
    });
});


/* Borrar proveedor por ID */
app.delete('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Servicio.findByIdAndRemove(id, (err, servicioBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar servicio',
                errors: err
            })
        }

        if (!servicioBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un servicio con ese ID',
                errors: { message: 'No existe un servicio con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            mensaje: 'Servicio borrado',
            servicio: servicioBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;