var nodemailer = require("nodemailer");
var hbs = require("nodemailer-express-handlebars");
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var Usuario = require("../models/usuario");
var Direccion = require("../models/direccion");
var Motorizado = require("../models/motorizado");
var usuarioData;
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;

// email sender function
exports.sendEmail = function(req, res, next) {

 // console.log(tipo);
  console.log('res',req);


  // var smtpConfig = {
  //   host: "smtpout.secureserver.net",
  //   port: 465,
  //   secure: true, // use SSL
  //   auth: {
  //     // user: "gestiones@fastzoom.cl",
  //     // pass: "laserena2019#"
  //       user: "saracual.r@gmail.com",
  //     pass: "playa los cocos"
  //   }
  // };



  let smtpConfig={
    service: 'gmail',
    auth: {
      user: "gestion.saracual@gmail.com",
      pass: "alananer10"
    }
};





  // if(!tipo){

  var email = req.body.email;
  var tipoSolicitud = req.body.tipoSolicitud;

  ////////////////////////////////////////
 // Configuracion para envio de correo //
////////////////////////////////////////

 

  // Creo transporter
  let transporter = nodemailer.createTransport(smtpConfig);
  const handlebarOptions = {
    viewEngine: {
      extName: ".handlebars",
      partialsDir: "./templates/",
      layoutsDir: "./templates/",
      defaultLayout: tipoSolicitud + ".handlebars"
    },
    viewPath: "./templates/",
    extName: ".handlebars"
  };

  transporter.use("compile", hbs(handlebarOptions));

  // verify connection configuration
  transporter.verify(function(error) {
    if (error) {
      console.log(error);
      return error;
    } else {
      console.log("Server is ready to take our messages");
    }
  });
  ////////////////////////////////////
 // Valido tipo de correo a enviar //
////////////////////////////////////

  if(tipoSolicitud === 'recuperarUsuario'){
    Usuario.find({ email: email }).exec((err, usuario) => {

if(err){
      return res.status(500).json({
        ok: false,
        mensaje: 'Error inesperado',
    })

  }


      if (usuario.length === 0) {
            return res.status(404).json({
                ok: false,
                mensaje: 'El usuario ' + email + ' no existe.',
            })
        };


        var token = jwt.sign({usuario : usuario[0]._id}, SEED, {expiresIn:600000}) //  10 minutos


      // Opciones del correo para recuperar usuario
        var mailOptions = {
          from: '"Fastzoom"<gestion.saracual@gmail.com>', // fast.zoom.2019@gmail.com
          to: email,
          subject: "Recuperar Contraseña",
          attachments: [
            {
              filename: "fast-zoom-logosinfondo.png",
              path: "./assets/fast-zoom-logosinfondo.png",
              cid: "gestion.saracual@gmail.com"
            }
          ],
          template: "recuperarUsuario",
          context: {
            data:usuario[0].nombre,
            ruta: "https://intense-dusk-29965.herokuapp.com/#/recuperar-usuario/"+ usuario[0]._id +'/'+ token
          }
        };

        // Enviamos el email
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              console.log('error envio', error);
              return error;
            } else {
  

              if(info.accepted.length > 0){
              req.body = 'Correo de recuperación enviado';
              }
  
              next();
            }
           });
          });

          }

        // }


// if(tipo === 'registroUsuario'){


//     // Creo transporter
//     let transporter = nodemailer.createTransport(smtpConfig);
//     const handlebarOptions = {
//       viewEngine: {
//         extName: ".handlebars",
//         partialsDir: "./templates/",
//         layoutsDir: "./templates/",
//         defaultLayout: "registroUsuario.handlebars"
//       },
//       viewPath: "./templates/",
//       extName: ".handlebars"
//     };
  
//     transporter.use("compile", hbs(handlebarOptions));
  
//     // verify connection configuration
//     transporter.verify(function(error) {
//       if (error) {
//         console.log(error);
//         return error;
//       } else {
//         console.log("Server is ready to take our messages");
//       }
//     });

//   // Definimos el email
//   var mailOptions = {
//     from: '"Fastzoom"<gestiones@fastzoom.cl>',
//     to: req.email,
//     subject: "Registro Fastzoom",
//     attachments: [
//       {
//         filename: "fast-zoom-logosinfondo.png",
//         path: "./assets/fast-zoom-logosinfondo.png",
//         cid: "gestiones@fastzoom.cl"
//       }
//     ],
//     template: "registroUsuario",
//     context: {
//       data: req,
//       ruta: "http://www.fastzoom.cl/#/activar/" + req._id
//     } // send extra values to

//   };


//     // Enviamos el email
//     transporter.sendMail(mailOptions, function(error, info) {
//       if (error) {
//         console.log(error);
//         return error;
//       } else {
//         console.log("Email sent");
//         return res.status(200).jsonp(req.body);
//       }
//     });
// }

}




  // // Busco datos de usuario segun id
  // Usuario.findById(req.usuario, (err, usuario) => {
  //   if (err) {
  //     return res.status(500).json({
  //       ok: false,
  //       mensaje: "Error inesperado.",
  //       error: err
  //     });
  //   }

    
  //     if (!usuario) {
  //       return res.status(404).json({
  //         ok: false,
  //         mensaje: "Error al validar usuario.",
  //         error: { message: "No existe un usuario con es ID" }
  //       });
  //     }

  //   usuarioData = usuario;

  //   // Busco direccion segun id
  //   Direccion.findById(req.direccionEntrega, (err, direccion) => {
  //     if (err) {
  //       return res.status(500).json({
  //         ok: false,
  //         mensaje: "Error al buscar direccion.",
  //         error: err
  //       });
  //     }

  //     // Busco motorizado segun id
  //     Motorizado.findById(req.encargadoDelivery, (err, motorizado) => {
  //       if (err) {
  //         return res.status(500).json({
  //           ok: false,
  //           mensaje: "Error inesperado.",
  //           error: err
  //         });
  //       }

  //       // Valido que tipo de correo se va enviar

  //       if (tipoSolicitud === "asignaMoto") {
  //         var mailOptions = {
  //           from: '"Fastzoom" <gestiones@fastzoom.cl>',
  //           to: email,
  //           subject: "Pedido FastZoom Asignado",
  //           attachments: [
  //             {
  //               filename: "fast-zoom-logosinfondo.png",
  //               path: "./assets/fast-zoom-logosinfondo.png",
  //               cid: "gestiones@fastzoom.cl"
  //             }
  //           ],
  //           template: "asignaMoto",
  //           context: {
  //             nControl: req.nControl,
  //             totalArticulos: req.totalArticulos,
  //             costoProducto: req.totalProductos,
  //             costoEnvio: req.totalDelivery,
  //             formaDePago: req.formaDePago,
  //             total: parseInt(req.totalProductos) + parseInt(req.totalDelivery),
  //             direccionText: direccion,
  //             motorizado,
  //             formaDePago: req.formaDePago
  //           } // send extra values to template
  //         };
  //       } else if (req.pedidos) {
  //         var mailOptions = {
  //           from: '"Fastzoom" <gestiones@fastzoom.cl>',
  //           to: req.email,
  //           subject: "Tu pedido Fastzoom",
  //           //  text: 'El usuario text ' + req.nControl + ' se ha sido creado con ¡Genial!.',
  //           //  html: ' <h3> Datos de tu pedido </h3> Numero de Control: ' + req.nControl + '<br> Numero de Articulos: ' + new Intl.NumberFormat().format(req.totalArticulos) + '<br> Costo de Producto:  ' + new Intl.NumberFormat().format(req.totalProductos) +' <br> Costo de Envio: ' + req.totalDelivery,
  //           attachments: [
  //             {
  //               filename: "fast-zoom-logosinfondo.png",
  //               path: "./assets/fast-zoom-logosinfondo.png",
  //               cid: "gestiones@fastzoom.cl"
  //             }
  //           ],
  //           template: "pedido",
  //           context: {
  //             nControl: req.nControl,
  //             totalArticulos: req.totalArticulos,
  //             costoProducto: req.totalProductos,
  //             costoEnvio: req.totalDelivery,
  //             formaDePago: req.formaDePago,
  //             total: parseInt(req.totalProductos) + parseInt(req.totalDelivery),
  //             direccionText: direccion,
  //             usuario: usuarioData
  //           } // send extra values to template
  //         };
  //       } else if (tipoSolicitud !== "recuperarUsuario") {
        

      
  //     });
  //   });
  // });
//};
