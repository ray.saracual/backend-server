var express = require('express');
var app = express();

var Direccion = require('../models/direccion');

var mdAutentication = require('../middlewares/autenticacion');

// Rutas 
/* Obtener Todas las Direcciones */
app.get('/', (req, res, next) => {
    Direccion.find({})
        .exec(
            (err, direccion) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando direccion',
                        errors: err
                    })
                } res.status(200).json({
                    ok: true,
                    direccion: direccion
                });
            })
});


/* Obtener Direcciones segun usuario */
app.get('/:id',mdAutentication.verificaToken, (req, res, next) => {

    var busqueda = req.params.id;

    console.log(busqueda);
 
    Direccion.find({_id:{$in:[busqueda]}},' direccion referencia lat lng',(err,direccion) => {
        if (err){ 
            return res.status(500).json({
                ok: false,
                mensaje: 'Error cargando direccion',
                errors: err
            })
            }else {
                res.status(200).json({
                    ok: true,
                    direccion: direccion
                });
            }
                });



});



/* Crear nueva direccion */
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var direccion = new Direccion({
        direccion: body.direccion,
        referencia: body.referencia,
        lat: body.lat,
        lng: body.lng,
        usuario: req.usuario._id,
    })



    direccion.save((err, direccionGuardada) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear dirección',
                errors: err
            })
        }
        res.status(201).json({
            ok: true,
            direccion: direccionGuardada,
            mensaje:'Dirección Registrada',
        });

    });

});


/* Borrar direccion por ID */
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Direccion.findByIdAndRemove(id, (err, direccionBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar direccion',
                errors: err
            })
        }

        if (!direccionBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un direccion con ese ID',
                errors: { message: 'No existe un direccion con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            mensaje: 'Dirección borrada',
            servicio: direccionBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;