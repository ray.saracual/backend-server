var express = require('express');


var app = express();
var Proveedor  = require ('../models/proveedor');

var Producto  = require ('../models/producto');

var Direccion  = require ('../models/direccion');

var Pedido  = require ('../models/pedido');
var mdAutentication = require('../middlewares/autenticacion');


/* Busqueda por Coleccion*/
app.get('/coleccion/:tabla/:busqueda', (req, res ) => {

    var busqueda = req.params.busqueda;
    var tabla =req.params.tabla;
    var regex = new  RegExp(busqueda,'i');

    switch(tabla){
        case'productos':
        promesa = buscarProducto(busqueda,regex)
        break;

        case'proveedores':
        promesa = buscarProveedor(busqueda,regex)
        break;

        case'direcciones':
        promesa = buscarDireccion(busqueda,regex)
        break;


        case'pedidos':
        promesa = buscarPedidos(busqueda,regex)
        break;

        default:
            return   res.status(400).json({      
                    ok: false,
                    mensaje : 'Tipo de busqueda no valido',                   
                });
    }

    promesa.then(data => {
        res.status(200).json({      
            ok: true,
            [tabla] : data,                   
        });
    })

});


/* Busqueda General*/

app.get('/todo/:busqueda', (req, res, next ) => {

    var busqueda = req.params.busqueda;
    var regex = new  RegExp(busqueda,'i');

    Promise.all([
        buscarProducto(busqueda, regex),
        buscarProveedor(busqueda, regex),
        buscarDireccion(busqueda, regex),
        buscarPedidos(busqueda, regex)  
    ])
    .then(respuestas => {
  
        res.status(200).json({      
            ok: true,
            Productos:respuestas[0],
            Proveedores : respuestas[1],
            Direcciones : respuestas[2],
            Pedidos : respuestas[3]
           
        });

    })  

});


// Funciones de busqueda

 function buscarProducto(busqueda, regex) {
    return new Promise( (resolve , reject ) =>{ 
        Producto.find({proveedor:regex})
            .populate('proveedor', 'nombre')
            .populate('categoria', 'nombre')

            .exec((err, productos) => {
                     if (err){
                         reject('Error al cargar productos', err);
                     }else {
                        resolve(productos)
                    }
                });
    
    });
}

function buscarProveedor(busqueda, regex) {
    return new Promise( (resolve , reject ) =>{ 
        Proveedor.find({_id:busqueda})
        .populate('direccion', 'lat lng direccion referencia')
          .exec((err, proveedor) => {
if (err){
     reject('Error', err);
    }else {
        resolve(proveedor)
    }
        });
    
    });
}   



function buscarDireccion(busqueda,regex ) {
    return new Promise((resolve, reject) =>{        
        Direccion.find({usuario:{$in:[busqueda]}},' direccion referencia lat lng',(err,direccion) => {
if (err){ 
     reject('Error', err);
    }else {
        resolve(direccion)
    }
        });
    
    });
}   



function buscarPedidos(busqueda,regex ) {
    return new Promise((resolve, reject) =>{        
        Pedido.find({"pedidos.proveedor":busqueda},(err,pedido) => {
if (err){ 
     reject('Error', err);
    }else {


           let dataout= [];

        pedido.forEach(data=>{
            console.log('aqui',data.pedidos);

            data.pedidos.forEach(element=> {

                if(element.proveedor !== busqueda ){

                    return;
                }

                dataout.push(element);


              
               
                // console.log('provee',element.proveedor);
            })

        



        })

         console.log('provee',pedido);

        resolve(dataout)
    }
        });
    
    });
}  


module.exports = app;





