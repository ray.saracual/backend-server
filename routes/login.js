var express = require('express');
var bcrypt = require('bcryptjs'); 
var jwt = require('jsonwebtoken')

var SEED = require('../config/config').SEED;

var app = express();
var Usuario = require('../models/usuario');

var mdAutenticacion = require('../middlewares/autenticacion')


app.post('/' , (req, res) => {

    var body = req.body;

  Usuario.findOne({ email: body.email}, (err,usuarioDB)=> {

    if (err) {
        return res.status(500).json({
            ok: false,
            mensaje: 'Error al buscar usuario',
            errors: err
        });
    }

    if(!usuarioDB){
        return res.status(400).json({
            ok: false,
            mensaje:'Credenciales Incorrectas - email',
            errors: err
        });
    }

    if (!bcrypt.compareSync(body.password , usuarioDB.password)){
       return res.status(400).json({
            ok: false,
            mensaje:'Credeciales Incorrectas - password',
            errors: err
        });
    }


    if (usuarioDB.valido===false){
        return res.status(400).json({
             ok: false,
             mensaje:'Su usuario no esta activo.',
             errors: err
         });
     }

    // Crear Token
    usuarioDB.password = ':)';
    var token = jwt.sign({usuario : usuarioDB}, SEED) // 4 horas 1440/60 
    
    res.status(200).json({
        ok: true,
        mensaje:'Login Correcto',
        usuario : usuarioDB,
        token:token,
        id: usuarioDB._id
    });

  })

    
});

app.get('/renovarToken',mdAutenticacion.verificaToken, (req, res) => {
    
    
var token = jwt.sign({usuario : req.usuario}, SEED, {expiresIn:10440}) // 4 horas 1440/60 

       
    res.status(200).json({
        ok: true,       
        token  : token 
    });


} ) ;

module.exports = app;