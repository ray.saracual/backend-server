var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categoriaSchema = new Schema({
    nombre: { type: String, require:[true, 'Categoria es necesaria']},
    img: { type:String, required:[ true, 'La imagen es necesaria']} 
},{ collection: 'categoria' });

module.exports = mongoose.model('Categoria', categoriaSchema)