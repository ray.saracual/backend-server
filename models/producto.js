var mongoose = require('mongoose');
var uniqueValidator =require('mongoose-unique-validator')
var Schema = mongoose.Schema;


var productoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    imagen: { type: String, require:[true, 'La imagen es necesaria']},
    categoria: {type: Schema.Types.ObjectId,ref:'Categoria', required: [true, 'La categoria es necesaria']},
    ingrediente: { type:Array, required: [true, 'Los ingredientes son necesarios'] },
    precio: { type: Number, required: [true, 'El precio es necesario'] },
    estatus:{type: Number,required:[true,'El estatus es necesario']},
},{ collection: 'producto' });


module.exports = mongoose.model('Producto', productoSchema)