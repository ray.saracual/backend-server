var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var origenPedidoSchema = new Schema({
    lat: { type: String, require:[true, 'Latitu es necesaria']},
    lng: { type:String, required:[ true, 'Longitud es necesaria']} 
},{ collection: 'origenPedido' });

module.exports = mongoose.model('OrigenPedido', origenPedidoSchema)