var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uniqueValidator =require('mongoose-unique-validator')


var motorizadoSchema = new Schema({
    nombreApellido: { type: String, require:[true, 'El nombre es necesario']},
    identificacion: { type: String, require:[true, 'La identificacion es necesaria'],unique:true},
    telefono: { type: String,  require:[true, 'El telefono es necesario'],unique:true}, 
    email: { type: String, required: [true, 'El email es necesario'],unique:true },
    clave: { type: String, required: [true, 'la clave es necesaria'] ,unique:true},
    fechaRegistro :{ type: String, required: [true, 'Fecha de registro es necesaria'] },
    vehiculo :{ type: String, required: [true, 'identificacion del vehiculo'],  unique:true},
    estatus : { type: Number, required: [true, 'El estatus es necesario'] },
});

motorizadoSchema.plugin(uniqueValidator, { message: 'Registro Duplicados {PATH}'})

module.exports = mongoose.model('Motorizado', motorizadoSchema)