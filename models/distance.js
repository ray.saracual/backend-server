var mongoose = require('mongoose');

var Schema = mongoose.Schema;



var distanceSchema = new Schema({
    origin: { type: String},
    destination: { type: String },
    distance: { type: Array},
    totalDistancia: { type: String },
});


module.exports = mongoose.model('Distance', distanceSchema)
