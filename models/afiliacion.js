var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uniqueValidator =require('mongoose-unique-validator')
var ObjectId = mongoose.Schema.Types.ObjectId;
var ProductoSchema = new Schema({ _id: ObjectId });


var afiliacionSchema = new Schema({
    codAfiliacion : { type:String,unique:true, required:[ true, 'codigo afiliacion es necesario']},
    servicio: {type: Schema.Types.ObjectId,ref:'Servicio', required: [true, 'Servicio es necesario']},
    proveedor:{type: Schema.Types.ObjectId,ref:'Proveedor', required: [true, 'Proveedor es necesario']}, 
    producto:[{type:Schema.Types.ObjectId, ref: 'Producto', required: [true, 'Producto es necesario']}],
    estatus : { type:String, required:[ true, 'Estatus es necesario']}
},{ collection: 'afiliacion' });

afiliacionSchema.plugin(uniqueValidator, { message: 'Codigo de Afiliacion duplicado'})

module.exports = mongoose.model('Afiliacion', afiliacionSchema)
