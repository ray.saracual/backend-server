var mongoose = require('mongoose');
var uniqueValidator =require('mongoose-unique-validator')
var Schema = mongoose.Schema;


var servicioSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },    
    img: { type: String, required: [true, 'El imagen es necesaria'] },
    estatus: { type: String, require:[true, 'El estatus es necesario']}, 
    observacion:{type: String,required:false}
},{ collection: 'servicio' });


module.exports = mongoose.model('Servicio', servicioSchema)