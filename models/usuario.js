var mongoose = require('mongoose');
var uniqueValidator =require('mongoose-unique-validator')
require('mongoose-type-email');
var Schema = mongoose.Schema;

var rolesValidos ={
    values:['ADMIN_ROLE','USER_ROLE'],
    message:'{VALUE} no es un rol permitido'
} 

var usuarioSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    telefono: { type: String, required: [true, 'El telefono es necesario'] },
    img: { type: String, require: false },
    email: { type: mongoose.SchemaTypes.Email,unique:true, required: [true, 'El correo es necesario'] },
    password: { type: String, required: [true, 'La contraseña es necesaria'] },
    rol: { type: String, require: true, default: 'USER_ROLE',enum:rolesValidos },
    valido: { type: Boolean, default: false }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El correo ya existe'})

module.exports = mongoose.model('Usuario', usuarioSchema)