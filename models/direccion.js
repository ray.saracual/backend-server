var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var direccionSchema = new Schema({
    direccion: { type: String, required: [true, 'La direccion es necesaria'] },    
    referencia: { type: String, required: [true, 'La referencia es necesaria'] },
    lat: { type: Number, required: [true, 'Latitud es necesaria'] },
    lng: { type: Number, required: [true, 'Longitud es necesaria'] },
    usuario:{type: Schema.Types.ObjectId,ref:'Usuario', required: [true, 'El Usuario es necesario']},

    
},{ collection: 'direccion' });


module.exports = mongoose.model('Direccion', direccionSchema)